
== Editing a board

=== Placement and drawing operations

Placement and drawing tools are located in the right toolbar.  When a tool is activated, it stays
active until a different tool is selected, or the tool is canceled with the `Esc` key. The
selection tool is always activated when any other tool is canceled.

Some toolbar buttons have more than one tool available in a palette.  These tools are indicated
with a small arrow in the lower-right corner of the button:
image:images/pcbnew_palette_buttons.png[]

To show the palette, you can click and hold the mouse button on the tool, or click and drag the
mouse.  The palette will show the most recently used tool when it is closed.

[width="100%",cols="5%,95%",]
|====
| image:images/icons/cursor.png[]
    | Selection tool (the default tool).
| image:images/icons/net_highlight.png[]
    | Net highlight tool: selecting pads, tracks, or vias with this tool will highlight the
      associated net while this tool is active.
| image:images/icons/tool_ratsnest.png[]
    | Local ratsnest tool: when the board ratsnest is hidden, selecting footprints with this tool
      will show the ratsnest for the selected footprint only. Selecting the same footprint again
      will hide its ratsnest. The local ratsnest setting for each footprint will remain in effect
      even after the local ratsnest tool is no longer active.
| image:images/icons/module.png[]
    | Footprint placement tool: click on the board to open the footprint chooser, then click again
      after choosing a footprint to confirm its location.
| image:images/icons/add_tracks.png[]

  image:images/icons/ps_diff_pair.png[]
    | Route tracks / route differential pairs: These tools activate the interactive router and
      allow placing tracks and vias.  The interactive router is described in more detail below.
| image:images/icons/add_via.png[]
    | Add vias: allows placing vias without routing tracks.
| image:images/icons/mw_add_line.png[]
    | Add microwave shapes: these tools allow creation of some types of high-frequency circuit
      elements such as stubs, arcs, and serpentines.
| image:images/icons/add_zone.png[]
    | Add filled zone: Click to set the start point of a zone, then configure its properties before
      drawing the rest of the zone outline.  Zone properties are described in more detail below.
| image:images/icons/add_keepout_area.png[]
    | Add rule area: Rule areas, formerly known as keepouts, can restrict the placement of items
      and the filling of zones, and can also define named areas to apply specific custom design
      rules to.
| image:images/icons/add_line.png[]
    | Draw lines.

    *Note:* Lines are graphical objects and are not the same as tracks placed with the Route Tracks
          tool.  Graphical objects cannot be assigned to a net.
| image:images/icons/add_arc.png[]
    | Draw arcs: pick the center point of the arc, then the start and end points.
| image:images/icons/add_rectangle.png[]
    | Draw rectangles. Rectangles can be filled or outlines.
| image:images/icons/add_circle.png[]
    | Draw circles. Circles can be filled or outlines.
| image:images/icons/add_graphical_polygon.png[]
    | Draw graphical polygons. Polygons can be filled our outlines.

    *Note:* Filled graphical polygons are not the same as filled zones: graphical polygons cannot
    be assigned to a net and will not keep clearance from other items.
| image:images/icons/text.png[]
    | Add text.
| image:images/icons/add_aligned_dimension.png[]

  image:images/icons/add_orthogonal_dimension.png[]

  image:images/icons/add_center_dimension.png[]

  image:images/icons/add_leader.png[]
    | Add dimensions. Dimension types are described in more detail below.
| image:images/icons/add_pcb_target.png[]
    | Add layer alignment mark.
| image:images/icons/delete.png[]
    | Deletion tool: click objects to delete them.
| image:images/icons/set_origin.png[]
    | Set drill/place origin. Used for fabrication outputs.
| image:images/icons/grid_select_axis.png[]
    | Set grid origin.

|====

=== Snapping

When moving, dragging, and drawing board elements, the grid, pads, and other elements can have
snapping points depending upon the settings in the user preferences.  In complex designs, snap
points can be so close together that it makes the current tool action difficult.  Both grid and
object snapping can be disabled while moving the mouse by using the modifier keys in the table
below.

[options="header",cols="40%,60%"]
|====
| Modifier Key | Effect
| `Shift` | Disable grid snapping.
| `Alt` | Disable object snapping.
|====

=== Editing object properties

All objects have properties that are editable in a dialog.  Use the hotkey `E` or select
Properties from the right-click context menu to edit the properties of selected item(s).  You can
only open the properties dialog if all the items you have selected are of the same type. To edit
the properties of different types of items at one time, see the section below on bulk editing
tools.

=== Working with footprints

NOTE: TODO: Write this section - covers footprint properties, updating from library, etc

=== Working with pads

NOTE: TODO: Write this section - covers pad properties

=== Working with zones

NOTE: TODO: Write this section

=== Graphical objects

Graphical objects (lines, arcs, rectangles, circles, polygons, and text) can exist on any layer but
cannot be assigned to a net.  Rectangles, circles, and polygons can be set to be filled or outlines
in their propeties dialogs.  The line width property will control the width of the outline even for
filled shapes.  Line width can be set to `0` for filled shapes to disable the outline.

==== Board outlines (Edge Cuts)

Pcbnew uses graphical objects on the `Edge.Cuts` layer to define the board outline. The outline
must be a continuous (closed) shape, but can be made up of different types of graphical object such
as lines and arcs, or be a single object such as a rectangle or polygon.  If no board outline is
defined, or the board outline is invalid, some functions such as the 3D viewer and some design rule
checks will not be functional.

=== Dimensions

NOTE: TODO: Write this section

=== Routing traces

NOTE: TODO: Write this section

==== Interactive router settings

NOTE: TODO: Write this section

==== Differential pairs

NOTE: TODO: Write this section

=== Forward and back annotation

NOTE: TODO: Write this section

==== Geographical re-annotation

NOTE: TODO: Write this section

=== Locking

Most objects can be locked through their properties dialogs or by using the right-click context
menu.  Locked objects cannot be selected unless the "Locked items" checkbox is enabled in the
selection filter. Attempting to move locked items will result in a warning dialog:

image::images/pcbnew_locked_items_dialog.png[]

Selecting "Override Locks" in this dialog will allow moving the locked items. Selecting "OK" will
allow you to move any unlocked items in the selection; leaving the locked items behind. Selecting
"Do not show again" will remember your choice for the rest of your session.

=== Bulk editing tools

NOTE: TODO: Write this section

=== Cleanup tools

NOTE: TODO: Write this section
